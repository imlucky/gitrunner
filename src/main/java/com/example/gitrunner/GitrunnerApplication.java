package com.example.gitrunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitrunnerApplication {

    public static void main(String[] args) {
        System.out.println("hello world");
        SpringApplication.run(GitrunnerApplication.class, args);
    }

}
